// Require library
var xl = require('excel4node');
module.exports = {

  excel: () => {
    var limit = 100;
    var now = new Date();
    var yesterday = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 1, 7, 0, 0, 0);
    console.log(yesterday);

    Lead.find({
        createdAt: {
          '>=': yesterday
        }
      })
      .populate('state').exec(function(err, leads) {
        if (err) {
          console.log(err);
        }
        console.log('Leads: ', leads.length);
        async.mapLimit(leads, limit, function(lead, callback) {
          Application.find({
            lead: lead.id
          }).populate('income').populate('profession').populate('companyPhone').exec(function(err, app) {
            if (err) {
              callback(err);
            } else {
              lead.application = (app[1]) ? app[1] : app[0];
              callback();
            }
          });
        }, function(err) {
          if (err) {
            console.log(err);
          } else {
            // Create a new instance of a Workbook class
            var wb = new xl.Workbook();

            // Add Worksheets to the workbook
            var ws = wb.addWorksheet('Sheet 1');

            // Headers
            ws.cell(1, 1).string('Teléfono');
            ws.cell(1, 2).string('Correo electrónico');
            ws.cell(1, 3).string('Ingreso mensual');
            ws.cell(1, 4).string('Comprobar ingresos');
            ws.cell(1, 5).string('Estatus laboral');
            // ws.cell(1, 6).string('Tarjeta de débito');
            ws.cell(1, 6).string('Fecha de nacimiento (D/M/Y)');
            ws.cell(1, 7).string('Estado');
            ws.cell(1, 8).string('Compañía telefónica');
            ws.cell(1, 9).string('Terminos y condiciones');
            ws.cell(1, 10).string('Aviso de privacidad');
            ws.cell(1, 11).string('Motivo préstamo');

            async.eachOfLimit(leads, limit, function(lead, index, cbb) {

              // console.log('-------------------- Lead : ', index, ' ----------------------');
              // Content
              let phone = lead.phone || '';
              // // console.log('phone: ', phone);
              ws.cell(index + 2, 1).string(phone);
              //
              let email = lead.email || '';
              // console.log('email: ', email);
              ws.cell(index + 2, 2).string(email);
              //
              let income = (lead.application && lead.application.income) ? lead.application.income.description : '';
              // // console.log('income: ', income);
              ws.cell(index + 2, 3).string(income);
              //
              let checkIncome = (lead.application && lead.application.checkIncome) ? 'Sí' : 'No';
              // // console.log('checkIncome: ', checkIncome);
              ws.cell(index + 2, 4).string(checkIncome);
              //
              let profession = (lead.application && lead.application.profession) ? lead.application.profession.description : '';
              // // console.log('profession: ', profession);
              ws.cell(index + 2, 5).string(profession);
              //
              // let debit = (lead.application && lead.application.debit) ? 'Sí' : 'No';
              // // console.log('debit: ', debit);
              // ws.cell(index + 2, 6).string(debit);
              //
              let birthDate = (lead.dayBirth) ? (lead.dayBirth + '/' + lead.monthBirth + '/' + lead.yearBirth) : '';
              // // console.log('birthDate: ', birthDate);
              ws.cell(index + 2, 6).string(birthDate);
              //
              let state = (lead.state) ? lead.state.description : '';
              // // console.log('state: ', state);
              ws.cell(index + 2, 7).string(state);

              let companyphone = (lead.application && lead.application.companyPhone) ? lead.application.companyPhone.description : '';
              // // console.log('companyphone: ', companyphone);
              ws.cell(index + 2, 8).string(companyphone);

              let termsConditions = (lead.application && lead.application.termsConditions) ? 'Sí' : 'No';
              // // console.log('termsConditions: ', termsConditions);
              ws.cell(index + 2, 9).string(termsConditions);

              let privacyNotice = (lead.application && lead.application.privacyNotice) ? 'Sí' : 'No';
              // // console.log('privacyNotice: ', privacyNotice);
              ws.cell(index + 2, 10).string(privacyNotice);

              let origin = lead.origin || '';
              // // console.log('origin: ', origin);
              ws.cell(index + 2, 11).string(origin);

              setTimeout(cbb, 0);

            }, function() {
              var filePath = 'Reporte.xlsx';
              wb.write(filePath);

              console.log('Excel complete, send to: ', sails.config.emailContact.report);

              var subject = 'Reporte diario WiniEs';
              var message = 'Reporte del día ' + yesterday + ' a la fecha. \n Total de leads nuevos: ' + leads.length;
              var attachments = (leads.length > 0) ? [{
                path: filePath
              }] : null;

              Email.send(subject, message, attachments, sails.config.emailContact.report, function(err) {
                if (err) {
                  console.log(err);
                } else {
                  console.log('Email sended');
                }
              });
            });
          }
        });
      });
  }

};